# MathSurfaces

1. This project explores some interesting math surface equations and renders them as 3d meshes and single surfaces inside Rhino 3d, using the Rhinocommon API.
2. The only limitation is that Rhino3d doesnot support native Python libraries such as Numpy, SciPy, etc. So we wrote a set of native functions which enable this locally inside IronPython.
3. The benefit of developing this inside Rhino 3d, is direct access to such surfaces natively inside a native 3d modeler. 
4. This further facilitates thickening/ shelling operations and direct access to 3d printing of these surfaces.

<details><summary> 1. Super Formula Surfaces</summary>

## SUPER FORMULA
### Super Formula Images

<img src="https://user-images.githubusercontent.com/6398561/145766254-513d5073-6f0e-4eed-b591-b5236f389e0b.jpg"  width="300" >
<img src="https://user-images.githubusercontent.com/6398561/145766265-a560ae8f-5707-4a54-9a61-ca146a04ca36.jpg"  width="300" >
<br>
<img src="https://user-images.githubusercontent.com/6398561/145766259-c627327d-f33f-4c07-bde3-4d456b014e30.jpg"  width="300" >
<img src="https://user-images.githubusercontent.com/6398561/145766266-256a907d-c6c6-4d35-89e2-3c5cc4ad319b.jpg"  width="300" >

---

### Super Formula Math Equations

```math
r(\varphi) = (|\frac{cos(\frac{m\varphi}{4})}{a}|^{n_2} + |\frac{sin(\frac{m\varphi}{4})}{b}|^{n_3})^{-\frac{1}{n_1}}
```

by choosing different values for the parameters $`a,b,m,n_1,n_2,n_3`$, different shapes can be generated.

It is possible to extend the formula to 3,4, $`n`$ dimensions, by means of the spherical product of superformulas.
The parametric equations are as follows:
```math
x = r_1(\theta)cos\theta \cdot r_2(\phi)cos\phi \\
y = r_1(\theta)sin\theta \cdot r_2(\phi)cos\phi \\
z = r_2(\phi)sin\phi \\
```

where 
```math
-\frac{\pi}{2} > {\phi} > \frac{\pi}{2} \\
-{\pi} > {\theta} > {\pi} \\
```
---

### Super Formula Python Code

<details><summary>Code</summary>

```py
# -*- coding: utf-8 -*-
"""
Super Shape 

EQUATION:

Extension to 3D using the spherical product.

x = r1(theta)Cos(theta)r2(omega)Cos(omega)
y = r1(theta)Sin(theta)r2(omega)Cos(omega)
z = r2(omega)Sin(omega)

-pi/2 lessThanEqualTo omega lessThanEqualTo +pi/2       eg. latitude
-pi lessThanEqualTo theta lessThanEqualTo pi            eg. longitude

WEB : http://paulbourke.net/geometry/supershape/
IMG : http://paulbourke.net/geometry/supershape/equation2.gif

00:39 13/07/2021
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
working!
02:21 13/07/2021

the angle domains were interchanged!! WTF!
03:15 13/07/2021
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
"""

import Rhino.Geometry as rg
import rhinoscriptsyntax as rs
import scriptcontext

import math
from math import *
import itertools
remapLst = lambda vLst, inLst, outLst : map(lambda v: (v - inLst[0]) * (outLst[1] - outLst[0]) / (inLst[1] - inLst[0]) + outLst[0],vLst)
lstMin = lambda v: min(v)   # analogus of array.min of DOTNET!
lstMax = lambda v: max(v)   # analogus of array.max of DOTNET!
rad = lambda v: math.radians(v)
deg = lambda v: math.degrees(v)

import System
watch = System.Diagnostics.Stopwatch()

def frange(x, y, num):
    """ 
    ensures the output lenght is the same as the requested num
    23:39 24/06/2021, 23:54 24/06/2021, 00:17 25/06/2021, 00:46 25/06/2021
    """
    num = num-1 
    # internally making the request 1 unit smaller as a division procedsure will 
    # increase the num of total elements by 1.
    # So we take care of this internally.
    
    #print "num: ", num
    #jump = float(Decimal((y-x)/num))
    jump = float((y-x)/num)
    ##############print "jump: ", jump
    ##############print x * jump
    
    #print y-x
    #print jump*num
    
    """
    while x <= y:
        #yield x
        frangeColl.append(x)
        x += jump
    """
    counter = itertools.count(start=0, step=jump)
    frangeColl = [x + next(counter) for i in range(num+1)]
    
    ##############print "frangeColl: ", frangeColl
    ##############print "len(frangeColl): ", len(frangeColl), "_ _ _ _"
    ##############print "frangeColl[0]: ", frangeColl[0]
    ##############print "frangeColl[-1]: ", frangeColl[-1]
    
    return frangeColl


remapLst = lambda vLst, inLst, outLst : map(lambda v: (v - inLst[0]) * (outLst[1] - outLst[0]) / (inLst[1] - inLst[0]) + outLst[0],vLst)
cartesianProduct = lambda v1, v2 : list(itertools.product(v1,v2))

chunks = lambda vL,vN : [vL[i:i + vN] for i in range(0, len(vL), vN)]

def quadMeshFromPtLstRC(ptLstRC):
    """
    nested ptLst to mesh!
    functioned | 02:47 15/06/2021
    """
    mMega = rg.Mesh()
    for i in range(0,len(ptLstRC)-1,1):
        localLst = ptLstRC[i]
        #print len(localLst)
        #print localLst[0]
        #####print "i:", i
        for j in range(0,len(localLst)-1,1):
            #####print "j:", j
            #print type(localLst[j])
            m = rg.Mesh()
            m.Vertices.Add(localLst[j])
            m.Vertices.Add(localLst[j+1])
            m.Vertices.Add(ptLstRC[i+1][j+1])
            m.Vertices.Add(ptLstRC[i+1][j])
            m.Faces.AddFace(0,1,2,3)
            m.Normals.ComputeNormals()
            m.Compact()
            mMega.Append(m)
            #scriptcontext.doc.Objects.AddMesh(m)
    
    mMega.Normals.ComputeNormals()
    #mMega.Normals.UnifyNormals()       03:07 13/07/2021
    mMega.Compact() 
    return mMega

def triMeshFromPtLstRC(ptLstRC):
    """
    nested ptLst to mesh!
    functioned | 02:47 15/06/2021
    functioned | 01:47 25/06/2021
    """
    mMega = rg.Mesh()
    for i in range(0,len(ptLstRC)-1,1):
        localLst = ptLstRC[i]
        for j in range(0,len(localLst)-1,1):
            
            m1 = rg.Mesh()
            m1.Vertices.Add(localLst[j])
            m1.Vertices.Add(localLst[j+1])
            m1.Vertices.Add(ptLstRC[i+1][j+1])
            m1.Faces.AddFace(0,1,2)
            m1.Normals.ComputeNormals()
            m1.Compact()
            
            m2 = rg.Mesh()
            m2.Vertices.Add(ptLstRC[i+1][j+1])
            m2.Vertices.Add(ptLstRC[i+1][j])
            m2.Vertices.Add(localLst[j])
            m2.Faces.AddFace(0,1,2)
            m2.Normals.ComputeNormals()
            m2.Compact()
            mMega.Append(m1)
            mMega.Append(m2)
    
    mMega.Normals.ComputeNormals()
    mMega.Compact()
    return mMega

ijLoop = lambda v1,v2: [(i, j) for i in v1 for j in v2]                         #SQUARE!
ijLoopByFunc = lambda v1,v2,func: [func(i, j) for i in v1 for j in v2]                         #SQUARE!

# =============================================================================

# https://blender.stackexchange.com/questions/194257/formula-according-to-blender-to-create-an-enneper-order-3
multiplier = 20 #0.0025  #85 #75
#r1 = 0.25 #0.5
#r2 = 8 #2

#x = r1(theta)Cos(theta)r2(omega)Cos(omega)
#y = r1(theta)Sin(theta)r2(omega)Cos(omega)
#z = r2(omega)Sin(omega)
#
#-pi/2 lessThanEqualTo omega lessThanEqualTo +pi/2       eg. latitude
#-pi lessThanEqualTo theta lessThanEqualTo pi            eg. longitude

"""
r1 = lambda angle, a=1, b=1, m=4, n1=0.50, n2=0.50, n3=4 : abs(abs(1 / a * abs(cos(m * angle / 4)))**n2 + abs(1 / b * abs(sin(m * angle / 4)))**n3)**(- 1 / n1)
r2 = lambda angle, a=1, b=1, m=4, n1=0.25, n2=0.25, n3=4 : abs(abs(1 / a * abs(cos(m * angle / 4)))**n2 + abs(1 / b * abs(sin(m * angle / 4)))**n3)**(- 1 / n1)

funcX = lambda theta,omega : ( r1(theta)*cos(theta)*r2(omega)*cos(omega) )*multiplier
funcY = lambda theta,omega : ( r1(theta)*sin(theta)*r2(omega)*cos(omega) )*multiplier
funcZ = lambda theta,omega : ( r2(omega)*sin(omega) )*multiplier
"""


#r1 = lambda angle, a=1, b=1, m=7, n1=0.2, n2=1.7, n3=1.7 : abs(abs(1 / a * abs(cos(m * angle / 4)))**n2 + abs(1 / b * abs(sin(m * angle / 4)))**n3)**(- 1 / n1)
#r1 = lambda angle, a=1, b=1, m=7, n1=2, n2=8, n3=4 : abs(abs(1 / a * abs(cos(m * angle / 4)))**n2 + abs(1 / b * abs(sin(m * angle / 4)))**n3)**(- 1 / n1)
#r1 = lambda angle, a=1, b=1, m=3, n1=30, n2=15, n3=15 : abs(abs(1 / a * abs(cos(m * angle / 4)))**n2 + abs(1 / b * abs(sin(m * angle / 4)))**n3)**(- 1 / n1)
#r1 = lambda angle, a=1, b=1, m=4, n1=0.5, n2=0.5, n3=4 : abs(abs(1 / a * abs(cos(m * angle / 4)))**n2 + abs(1 / b * abs(sin(m * angle / 4)))**n3)**(- 1 / n1)
r1 = lambda angle, a=1, b=1, m=8, n1=60, n2=100, n3=30 : abs(abs(1 / a * abs(cos(m * angle / 4)))**n2 + abs(1 / b * abs(sin(m * angle / 4)))**n3)**(- 1 / n1)
r2 = lambda angle, a=1, b=1, m=8, n1=0.5, n2=10, n3=10 : abs(abs(1 / a * abs(cos(m * angle / 4)))**n2 + abs(1 / b * abs(sin(m * angle / 4)))**n3)**(- 1 / n1)

#funcX = lambda theta,omega : ( r1(theta)*cos(theta)*r2(omega)*cos(omega) )*multiplier
#funcY = lambda theta,omega : ( r1(theta)*sin(theta)*r2(omega)*cos(omega) )*multiplier
funcX = lambda theta,omega : ( r1(theta)*cos(theta)*r2(omega)*cos(omega) )*multiplier
funcY = lambda theta,omega : ( r1(theta)*sin(theta)*r2(omega)*cos(omega) )*multiplier
funcZ = lambda theta,omega : ( r2(omega)*sin(omega) )*multiplier

# ------------------------------------------------------------------------------

def bakeMesh(numMake, radialFlare): 
    
    #===============================================THIS IS THE PRINT RESOLUTION
    #numMake = 100 #250 #100 #50 #150 #152 #130 #100 #85 #60 #200 #100
    
    
    #radialFlare =  40 #50 #55 #60 # 61  # 63 #64 #62 #60 #55 #65
    #u = frange(0.0025,radialFlare, numMake)
    u = frange(50,radialFlare, numMake)
    v = frange(0,360, numMake)
    uRAD = map(rad,u)
    vRAD = map(rad,v)
    
    cartesianFuncMap = lambda func, v1,v2: [func(x,y) for x in v1 for y in v2]
    x = cartesianFuncMap(funcX,uRAD,vRAD)
    y = cartesianFuncMap(funcY,uRAD,vRAD)
    #z = map(funcZ,*[x,y])
    z = cartesianFuncMap(funcZ,uRAD,vRAD)
    
    # the OVEN!
    pt = map(rg.Point3d,*[x,y,z])
    #ptsID = map(scriptcontext.doc.ActiveDoc.Objects.AddPoint,pt)
    #rs.AddObjectsToGroup(ptsID, rs.AddGroup())
    ptChunks = chunks(pt,numMake)
    
    #mRC = quadMeshFromPtLstRC(ptChunks)
    mRC = triMeshFromPtLstRC(ptChunks)
    
    mID = scriptcontext.doc.ActiveDoc.Objects.AddMesh(mRC)
    return mRC

def bakeCrv(numMake, radialFlare): 
    
    #===============================================THIS IS THE PRINT RESOLUTION
    #numMake = 100 #250 #100 #50 #150 #152 #130 #100 #85 #60 #200 #100
    
    
    #radialFlare =  40 #50 #55 #60 # 61  # 63 #64 #62 #60 #55 #65
    #u = frange(0.0025,radialFlare, numMake)
    u = frange(0.005,radialFlare, numMake)
    v = frange(0,360, numMake)
    uRAD = map(rad,u)
    vRAD = map(rad,v)
    
    cartesianFuncMap = lambda func, v1,v2: [func(x,y) for x in v1 for y in v2]
    x = cartesianFuncMap(funcX,uRAD,vRAD)
    y = cartesianFuncMap(funcY,uRAD,vRAD)
    #z = map(funcZ,*[x,y])
    z = cartesianFuncMap(funcZ,uRAD,vRAD)
    
    # the OVEN!
    pt = map(rg.Point3d,*[x,y,z])
    #ptsID = map(scriptcontext.doc.ActiveDoc.Objects.AddPoint,pt)
    #rs.AddObjectsToGroup(ptsID, rs.AddGroup())
    ptChunks = chunks(pt,numMake)
    
    # tried to add an extra closing point on the point list, but to avail.
    # still produces a kink at the start and end points...
    map(lambda v: v.append(v[-1]),ptChunks)
    
    #mRC = quadMeshFromPtLstRC(ptChunks)
    ##mRC = triMeshFromPtLstRC(ptChunks)
    
    
    #crvRC = map(lambda v: rg.NurbsCurve.Create(False ,1,v),ptChunks)
    #crvRC = map(lambda v: rg.NurbsCurve.Create(False ,3,v),ptChunks)
    crvRC = map(lambda v: rg.NurbsCurve.Create(False ,5,v),ptChunks)
    crvID = map(scriptcontext.doc.ActiveDoc.Objects.AddCurve,crvRC)
    rs.AddObjectsToGroup(crvID,rs.AddGroup())
    
    """ # add a set of curves in the opp. direction!
    ptChunks2 = zip(*ptChunks)
    crvRC2 = map(lambda v: rg.NurbsCurve.Create(False ,5,v),ptChunks2)
    crvID2 = map(scriptcontext.doc.ActiveDoc.Objects.AddCurve,crvRC2)
    rs.AddObjectsToGroup(crvID2,rs.AddGroup())
    """
    
    #mID = scriptcontext.doc.ActiveDoc.Objects.AddMesh(mRC)
    #return mRC
    crvRC

def bakePts(numMake, radialFlare): 
    # 03:01 28/06/2021
    #===============================================THIS IS THE PRINT RESOLUTION
    #numMake = 100 #250 #100 #50 #150 #152 #130 #100 #85 #60 #200 #100
    
    
    #radialFlare =  40 #50 #55 #60 # 61  # 63 #64 #62 #60 #55 #65
    #u = frange(0.0025,radialFlare, numMake)
    
    #u = frange(-90,+90, numMake)
    #v = frange(-180,+180, numMake)
    u = frange(-180,+180, numMake)
    v = frange(-90,+90, numMake)
    uRAD = map(rad,u)
    vRAD = map(rad,v)
    
    cartesianFuncMap = lambda func, v1,v2: [func(x,y) for x in v1 for y in v2]
    x = cartesianFuncMap(funcX,uRAD,vRAD)
    y = cartesianFuncMap(funcY,uRAD,vRAD)
    #z = map(funcZ,*[x,y])
    z = cartesianFuncMap(funcZ,uRAD,vRAD)
    
    # the OVEN!
    pt = map(rg.Point3d,*[x,y,z])
    #ptsID = map(scriptcontext.doc.ActiveDoc.Objects.AddPoint,pt)
    #rs.AddObjectsToGroup(ptsID, rs.AddGroup())
    ptChunks = chunks(pt,numMake)
    
    """
    for i,j in enumerate(ptChunks):
        ptLocal = map(scriptcontext.doc.ActiveDoc.Objects.AddPoint,j)
        rs.AddObjectsToGroup(ptLocal,rs.AddGroup())
    """
    
    mRC = quadMeshFromPtLstRC(ptChunks)
    #mRC = triMeshFromPtLstRC(ptChunks)
    mID = scriptcontext.doc.ActiveDoc.Objects.AddMesh(mRC)
    
    """
    # tried to add an extra closing point on the point list, but to avail.
    # still produces a kink at the start and end points...
    map(lambda v: v.append(v[-1]),ptChunks)
    
    #mRC = quadMeshFromPtLstRC(ptChunks)
    ##mRC = triMeshFromPtLstRC(ptChunks)
    
    
    #crvRC = map(lambda v: rg.NurbsCurve.Create(False ,1,v),ptChunks)
    #crvRC = map(lambda v: rg.NurbsCurve.Create(False ,3,v),ptChunks)
    crvRC = map(lambda v: rg.NurbsCurve.Create(False ,5,v),ptChunks)
    crvID = map(scriptcontext.doc.ActiveDoc.Objects.AddCurve,crvRC)
    rs.AddObjectsToGroup(crvID,rs.AddGroup())
    """
    
    """ # add a set of curves in the opp. direction!
    ptChunks2 = zip(*ptChunks)
    crvRC2 = map(lambda v: rg.NurbsCurve.Create(False ,5,v),ptChunks2)
    crvID2 = map(scriptcontext.doc.ActiveDoc.Objects.AddCurve,crvRC2)
    rs.AddObjectsToGroup(crvID2,rs.AddGroup())
    """
    
    #mID = scriptcontext.doc.ActiveDoc.Objects.AddMesh(mRC)
    #return mRC
    #return crvRC
    return ptChunks #nested ptList aka "a point grid"

def bakeSrf(numMake, radialFlare): 
    # 03:05 28/06/2021
    #===============================================THIS IS THE PRINT RESOLUTION
    #numMake = 100 #250 #100 #50 #150 #152 #130 #100 #85 #60 #200 #100
    
    
    #radialFlare =  40 #50 #55 #60 # 61  # 63 #64 #62 #60 #55 #65
    #u = frange(0.0025,radialFlare, numMake)
    u = frange(0.005,radialFlare, numMake)
    v = frange(0,360, numMake)
    uRAD = map(rad,u)
    vRAD = map(rad,v)
    
    cartesianFuncMap = lambda func, v1,v2: [func(x,y) for x in v1 for y in v2]
    x = cartesianFuncMap(funcX,uRAD,vRAD)
    y = cartesianFuncMap(funcY,uRAD,vRAD)
    #z = map(funcZ,*[x,y])
    z = cartesianFuncMap(funcZ,uRAD,vRAD)
    
    # the OVEN!
    pt = map(rg.Point3d,*[x,y,z])
    #ptsID = map(scriptcontext.doc.ActiveDoc.Objects.AddPoint,pt)
    #rs.AddObjectsToGroup(ptsID, rs.AddGroup())
    ptChunks = chunks(pt,numMake)
    
    """
    for i,j in enumerate(ptChunks):
        ptLocal = map(scriptcontext.doc.ActiveDoc.Objects.AddPoint,j)
        rs.AddObjectsToGroup(ptLocal,rs.AddGroup())
    """
    
    srfRC = rg.NurbsSurface.CreateThroughPoints(pt,numMake,numMake,3,3,False,False)
    srfID = scriptcontext.doc.ActiveDoc.Objects.AddSurface(srfRC)
    
    """
    # tried to add an extra closing point on the point list, but to avail.
    # still produces a kink at the start and end points...
    map(lambda v: v.append(v[-1]),ptChunks)
    
    #mRC = quadMeshFromPtLstRC(ptChunks)
    ##mRC = triMeshFromPtLstRC(ptChunks)
    
    
    #crvRC = map(lambda v: rg.NurbsCurve.Create(False ,1,v),ptChunks)
    #crvRC = map(lambda v: rg.NurbsCurve.Create(False ,3,v),ptChunks)
    crvRC = map(lambda v: rg.NurbsCurve.Create(False ,5,v),ptChunks)
    crvID = map(scriptcontext.doc.ActiveDoc.Objects.AddCurve,crvRC)
    rs.AddObjectsToGroup(crvID,rs.AddGroup())
    """
    
    """ # add a set of curves in the opp. direction!
    ptChunks2 = zip(*ptChunks)
    crvRC2 = map(lambda v: rg.NurbsCurve.Create(False ,5,v),ptChunks2)
    crvID2 = map(scriptcontext.doc.ActiveDoc.Objects.AddCurve,crvRC2)
    rs.AddObjectsToGroup(crvID2,rs.AddGroup())
    """
    
    #mID = scriptcontext.doc.ActiveDoc.Objects.AddMesh(mRC)
    #return mRC
    #return crvRC
    #return ptChunks #nested ptList aka "a point grid"
    return srfRC 

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
watch.Start()
rs.EnableRedraw(False)

numMake = 100 #200 #50 #250 #150 #350 #280 #250 #100 #50 #150 #152 #130 #100 #85 #60 #200 #100
#radialFlareLst =  [50, 55]       # ITERATOR-mini!
#radialFlareLst =  [40,50,55,58,60,61,62,63,64,65,66,67,68,69,70]       # ITERATOR!
radialFlareLst =  [5]    #[62.5]
#generatedIterations = [bakeMesh(numMake, i) for i in radialFlareLst]
#generatedIterations = [bakeCrv(numMake, i) for i in radialFlareLst]
generatedIterations = [bakePts(numMake, i) for i in radialFlareLst]
#generatedIterations = [bakeSrf(numMake, i) for i in radialFlareLst]

rs.EnableRedraw(True)
watch.Stop()

print "> ............................................................................................"
print "> Print Resolution: {}".format(numMake)
#print "> Number of Triangle Mesh Faces per Iteration: {}".format(generatedIterations[0].Faces.Count)
print "> Number of Triangle Mesh Faces per Iteration: {}".format(len(generatedIterations))
print "> Number of Iterations Generated: {}".format(len(radialFlareLst))

#print "> Execution Time: {} seconds".format(watch.ElapsedMilliseconds/1000)
elapsedSeconds = watch.ElapsedMilliseconds/1000
elapsedHMSformat = System.TimeSpan.FromSeconds(elapsedSeconds)
print "> Execution Time: {} (HH:MM:SS)".format(elapsedHMSformat)
print "> ............................................................................................"

scriptcontext.doc.Views.Redraw()
```
</details>

---

### Super Formula Images 2
<details><summary>State 2</summary>
![__210714 04_typeB](https://user-images.githubusercontent.com/6398561/145766265-a560ae8f-5707-4a54-9a61-ca146a04ca36.jpg)
![__210714 04_typeA](https://user-images.githubusercontent.com/6398561/145766266-256a907d-c6c6-4d35-89e2-3c5cc4ad319b.jpg)
</details>
<details><summary>Grasshopper Python component</summary>
![Capture_GH1](https://user-images.githubusercontent.com/6398561/145766294-8d617814-e3dc-4403-a099-ae6b158f7148.JPG)
</details>
<details><summary>Variations</summary>
![210715 04_colorRange](https://user-images.githubusercontent.com/6398561/145766302-f9cf63c7-2f45-4e1e-9e7d-92407af6e3f9.jpg)
![210715 01_transparentPlasticSurface](https://user-images.githubusercontent.com/6398561/145766303-2457de87-f30b-4c91-b10c-c98eb11bd364.jpg)
![210715 01_hiRes](https://user-images.githubusercontent.com/6398561/145766305-f4f5261f-4f7e-4ff1-945d-d7806a44ddf4.jpg)
![210714 04](https://user-images.githubusercontent.com/6398561/145766307-1d491725-c307-4a22-8fff-52b054ad5f68.jpg)
![210714 02](https://user-images.githubusercontent.com/6398561/145766309-6a5a0387-b074-4fc9-86c3-12f058c7bc77.jpg)
</details>

---
</details>

